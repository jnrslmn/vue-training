class Animal {
  constructor () {
    this.type = 'animal'
    this.movement = 'walk'
  }

  getMovement () {
    return this.movement
  }

  static eat () {
    return 'with mouth'
  }

  toString () {
    const { type } = this
    return `Ini adalah ${type} class`
  }
}

class Bird extends Animal {
  constructor() {
    super()
  }

  setMovement() {
    this.movement = 'Fly'
  }

  static fly() {
    return true
  }
}
